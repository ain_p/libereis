﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Services.Protocols;
using LibeReis.Models;
using LibeReis.Services;
using Newtonsoft.Json;

namespace LibeReis.Controllers
{
    [Authorize]
    public class TripsController : MyController
    {
        private ICategoryService categoryService;

        // GET: Trips
        public ActionResult Index()
        {
            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            ViewBag.FullName = $"{CurrentPerson.FirstName} {CurrentPerson.LastName}";
            if (CurrentPerson != null)
            {
                var trips = db.Trips
                    .Where(t => t.People.Any(s => s.Id == CurrentPerson.Id))
                //.Include(t => t.TripCategory)
                //.Include(t => t.TripSubcategory)
                //.Include(t => t.People)
                .ToList();
                if (trips != null)
                {
                    Dictionary<int, int> Durations = new Dictionary<int, int>();
                    foreach (Trip t in trips)
                    {
                        Durations.Add(t.Id, ((t.EndDate - t.StartDate)?.Days + 1) ?? 0);
                    }
                    ViewBag.Durations = Durations;
                }
                return View(trips);
            }
            return View();
        }

        // GET: Trips/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            ViewBag.FullName = $"{CurrentPerson.FirstName} {CurrentPerson.LastName}";
            var trip = db.Trips
                .Where(t => t.Id == id && t.People.Any(p => p.Id == CurrentPerson.Id))
                .Include(t => t.People)
                .Include(t => t.TripCategory)
                .Include(t => t.TripSubcategory)
                .Include(t => t.TripDays)
                .Include(t=>t.TripFoodItems)
                .FirstOrDefault()
                ;

            if (trip == null)
            {
                return HttpNotFound();
            }
            ViewBag.FoodTypes = TripFoodItem.FoodTypes;
            return View(trip);
        }

        // GET: Trips/Create
        public ActionResult Create()
        {
            CheckPerson();
            ViewBag.TripCategoryId = new SelectList(db.TripCategories.OrderBy(x => x.Name), "Id", "Name");
            ViewBag.TripSubcategoryId = new SelectList(db.TripSubcategories.OrderBy(x => x.Name), "Id", "Name");
            return View(new Trip { Created = DateTime.Now, OwnerId = CurrentPerson.Id });
        }

        // POST: Trips/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Created,StartDate,OwnerId,EndDate,TripSubcategoryId,TripCategoryId")] Trip trip)
        {
            CheckPerson();
            if (ModelState.IsValid)
            {
                trip.EndDate = trip.StartDate;
                db.Trips.Add(trip);
                db.SaveChanges();
                trip.TripDays.Add(new TripDay { TripDay1 = trip.StartDate.Value, Name = "Day 1" });
                trip.People.Add(db.People.FirstOrDefault(p => p.Id == trip.OwnerId));
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TripCategoryId = new SelectList(db.TripCategories, "Id", "Name", trip.TripCategoryId);
            ViewBag.TripSubcategoryId = new SelectList(db.TripSubcategories, "Id", "Name", trip.TripSubcategoryId);
            return View(trip);
        }

        // GET: Trips/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trip trip = db.Trips.Find(id);
            if (trip == null)
            {
                return HttpNotFound();
            }
            ViewBag.TripCategoryId = new SelectList(db.TripCategories, "Id", "Name", trip.TripCategoryId);
            ViewBag.TripSubcategoryId = new SelectList(db.TripSubcategories, "Id", "Name", trip.TripSubcategoryId);
            return View(trip);
        }

        // POST: Trips/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,OwnerId,Created,StartDate,EndDate,TripSubcategoryId,TripCategoryId")] Trip trip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trip).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TripCategoryId = new SelectList(db.TripCategories, "Id", "Name", trip.TripCategoryId);
            ViewBag.TripSubcategoryId = new SelectList(db.TripSubcategories, "Id", "Name", trip.TripSubcategoryId);
            return View(trip);
        }

        // GET: Trips/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckPerson();
            Trip trip = db.Trips.Find(id);
            if (trip == null || trip.OwnerId != CurrentPerson.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.CurrentPerson = CurrentPerson;
            return View(trip);
        }

        // POST: Trips/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Trip trip = db.Trips.Find(id);
            if (trip != null)
            {
                trip.People.Clear();
                db.Spendings.RemoveRange(trip.Spendings);
                db.TripDays.RemoveRange(trip.TripDays);
                db.TripEquipments.RemoveRange(trip.TripEquipments);
                db.TripFoodItems.RemoveRange(trip.TripFoodItems);
                db.TodoLists.RemoveRange(trip.TodoLists);
                db.Trips.Remove(trip);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        // GET: Trips/AddDay/5
        public ActionResult AddDay(int id)
        {
            CheckPerson();
            var trip = db.Trips
                .FirstOrDefault(t => t.Id == id && t.OwnerId == CurrentPerson.Id)
                ;
            if (trip == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            trip.EndDate = trip.EndDate?.AddDays(1);
            trip.TripDays.Add(new TripDay
            {
                TripDay1 = trip.EndDate.Value,
                Name = $"Day {(trip.EndDate - trip.StartDate)?.Days + 1}"
            });
            db.SaveChanges();
            return RedirectToAction("Details", new { id });
        }

        public ActionResult DeleteLastDay(int id)
        {
            CheckPerson();
            Trip trip = db.Trips.Find(id);
            if (trip == null || trip.OwnerId != CurrentPerson.Id) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            if (trip.TripDays.Count() == 1)
            {
                trip.TripDays.Remove(trip.TripDays.Single());
                db.SaveChanges();
                db.Trips.Remove(trip);
                db.SaveChanges();
                return RedirectToAction("Index", "Trips");
            }
            TripDay day = trip.TripDays.OrderBy(x => x.TripDay1).Last();
            day.TripFoodItems.Clear();
            db.TripDays.Remove(day);
            trip.EndDate = trip.EndDate?.AddDays(-1);
            db.SaveChanges();
            return RedirectToAction("Details", new { id });
        }

        //// GET: Trips/SubCategories/5
        //public JsonResult SubCategories(int tripCategoryId)
        //{
        //    return Json(categoryService.GetSubCategories(tripCategoryId));
        //}

        public string Subcategories(int catId)
        {
            var subcat = db.TripSubcategories.Where(x => x.CategoryId == catId)
                    .Select(x => new { Id = x.Id, Name = x.Name })
                .ToList()
                ;
            var json = JsonConvert.SerializeObject(subcat);
            return json; db.Trips.Where(t => t.Id == 2).Include(t => t.TripFoodItems); db.TripDays.Where(t => t.Id == 2).Include(t => t.TripFoodItems);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
