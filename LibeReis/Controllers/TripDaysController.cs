﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibeReis.Models;

namespace LibeReis.Controllers
{
    [Authorize]
    public class TripDaysController : MyController
    {
        //private ReisEntities db = new ReisEntities();

        // GET: TripDays
        public ActionResult Index()
        { 
                return View(db.TripDays.ToList());
        }

        // GET: TripDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripDay tripDay = db.TripDays.Find(id);
            if (tripDay == null)
            {
                return HttpNotFound();
            }
            return View(tripDay);
        }

        // GET: TripDays/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TripDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TripDay1,Name,Description")] TripDay tripDay)
        {
            if (ModelState.IsValid)
            {
                db.TripDays.Add(tripDay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tripDay);
        }

        // GET: TripDays/Edit/5
        public ActionResult Edit(int? id)
        {
            CheckPerson();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripDay tripDay = db.TripDays.Find(id);
            if (tripDay.Trip.OwnerId != CurrentPerson.Id) tripDay = null;
            if (tripDay == null)
            {
                return HttpNotFound();
            }
            return View(tripDay);
        }

        // POST: TripDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TripId,TripDay1,Name,Description")] TripDay tripDay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tripDay).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("Details", "Trips", new { id = tripDay.TripId });
            }
            return View(tripDay);
        }

        // GET: TripDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripDay tripDay = db.TripDays.Find(id);
            if (tripDay == null)
            {
                return HttpNotFound();
            }
            return View(tripDay);
        }

        // POST: TripDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TripDay tripDay = db.TripDays.Find(id);
            db.TripDays.Remove(tripDay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
