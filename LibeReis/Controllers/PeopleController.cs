﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibeReis.Models;

namespace LibeReis.Controllers
{
    [Authorize]
    public class PeopleController : MyController
    {

        // GET: People
        public ActionResult Index(int? id)
        {
            IQueryable<Person> people;
            CheckPerson();
            ViewBag.CurrentPersonId = CurrentPerson.Id;
            if (id == null)
            {
                people = db.People
                    .Where(t => t.Id == CurrentPerson.Id)
                    ;
                return View(people.ToList());
            }
            else
            {
                people = db.People
                    .Where(t => t.Trips.Any(p => p.Id == id))
                    ;
                if (people.Any(t => t.Id == CurrentPerson.Id))
                {
                    ViewBag.TripId = id;
                    ViewBag.OwnerId = GetOwnerId(id);
                    return View(people.ToList());
                }
            }
            return RedirectToAction("Details", "People", new { id = CurrentPerson.Id });
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create(int? id)
        {
            CheckPerson();
            if (id == null || GetOwnerId(id) != CurrentPerson.Id)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");
            ViewBag.TripId = id;
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id, Email")] Person person, int id)
        {
            var trip = db.Trips.Find(id);
            var member = db.People.Where(t => t.Email == person.Email).FirstOrDefault();
            if (member == null) ModelState.AddModelError("Email", "Person with this Email in not a user");
            if (member != null)
            {
                if (trip.People.Any(t => t.Id == member.Id)) ModelState.AddModelError("Email", "Person with this Email is already a trip member");
            }
            if (ModelState.IsValid)
            {
                trip.People.Add(member);
                db.SaveChanges();
                return RedirectToAction("Index", "People", new { id });
            }
            ViewBag.TripId = id;
            //ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id, int? tripId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.TripId = tripId;
            //ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,FirstName,LastName,PhoneNumber,PictureId")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.Entry(person).Property("PictureId").IsModified = false;
                person.PhoneNumber = person.PhoneNumber ?? "";
                db.SaveChanges();
                ChangeDataFile(
                file,
                x => { person.PictureId = x; db.SaveChanges(); },
                person.PictureId);
                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int id, int? tripId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.TripId = tripId;
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int? tripId)
        {
            var trip = db.Trips.Find(tripId);
            var person = db.People.Find(id);
            trip.People.Remove(person);

            //Person person = db.People.Find(id);
            //db.People.Remove(person);
            ViewBag.TripId = tripId;
            db.SaveChanges();
            return RedirectToAction("Index", new { id = ViewBag.TripId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
