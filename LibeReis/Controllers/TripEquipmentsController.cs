﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibeReis.Models;


namespace LibeReis.Controllers
{
    [Authorize]
    public class TripEquipmentsController : MyController
    {

        // GET: TripEquipments
        public ActionResult Index( int id)
        {
            CheckPerson();
            var tripEquipments = db.TripEquipments.Include(t => t.EquipmentType).Include(t => t.Trip)
                 .Where(t => t.TripId == id && t.Trip.People.Any(p => p.Id == CurrentPerson.Id))
                .ToList();
            ViewBag.CurrentPersonId = CurrentPerson.Id;
            ViewBag.OwnerId = GetOwnerId(id);
            ViewBag.TripId = id;

            ViewBag.Owners = db.People.Select(x => new { x.Id, Name = x.FirstName + " " + x.LastName}).ToDictionary(x => x.Id,x => x.Name);
            ViewBag.Owners.Add(0, "");
            return View(tripEquipments);
          
        }

        // GET: TripEquipments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            ViewBag.FullName = $"{CurrentPerson.FirstName} {CurrentPerson.LastName}";
            var trip = db.Trips
                .Where(t => t.Id == id && t.People.Any(p => p.Id == CurrentPerson.Id))
                //.Include(t => t.People)
                //.Include(t => t.TripCategory)
                //.Include(t => t.TripSubcategory)
                //.Include(t => t.TripDays)
                .FirstOrDefault()
                ;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripEquipment tripEquipment = db.TripEquipments.Find(id);
            if (tripEquipment == null)
            {
                return HttpNotFound();
            }
            return View(tripEquipment);
        }

        // GET: TripEquipments/Create
        public ActionResult Create(int id)
        {
            CheckPerson();
            var ownerId = GetOwnerId(id);
            Trip trip = db.Trips.Find(id);
            if (trip == null) return HttpNotFound();
           
             
            ViewBag.EquipmentId = new SelectList(db.EquipmentTypes, "Id", "Name");
            ViewBag.PersonId = new SelectList(trip.People, "Id", "FullName");

            
            return View(new TripEquipment { TripId = id } );
        }

        // POST: TripEquipments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,EquipmentId,TripId,PersonId,CarrierId,Amount,Weight,EquipmentName")] TripEquipment tripEquipment)
        {
            if (ModelState.IsValid)
            {
                db.TripEquipments.Add(tripEquipment);
                db.SaveChanges();
                return RedirectToAction("Index", "TripEquipments", new { id = tripEquipment.TripId});
            }

            ViewBag.EquipmentId = new SelectList(db.EquipmentTypes, "Id", "Name", tripEquipment.EquipmentId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FirstName", tripEquipment.PersonId);
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", tripEquipment.TripId);
            return View(tripEquipment);
        }

        // GET: TripEquipments/Edit/5
        public ActionResult Edit(int id)
        {

            TripEquipment tripEquipment = db.TripEquipments.Find(id);

            //Person person = db.People.Find(id);

            ViewBag.EquipmentId = new SelectList(db.EquipmentTypes, "Id", "Name", tripEquipment.EquipmentId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "Fullname", tripEquipment.PersonId);
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", tripEquipment.TripId);

            tripEquipment.EquipmentName = tripEquipment.EquipmentName.Trim();
            return View(tripEquipment);
        }

        // POST: TripEquipments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EquipmentId,TripId,PersonId,CarrierId,Amount,Weight,EquipmentName")] TripEquipment tripEquipment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tripEquipment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "TripEquipments", new { id = tripEquipment.TripId});
            }
            ViewBag.EquipmentId = new SelectList(db.EquipmentTypes, "Id", "Name", tripEquipment.EquipmentId);
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", tripEquipment.TripId);
           
            return View("Index");
        }

        // GET: TripEquipments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripEquipment tripEquipment = db.TripEquipments.Find(id);
            if (tripEquipment == null)
            {
                return HttpNotFound();
            }
            ViewBag.TripId = tripEquipment.TripId;
            return View(tripEquipment);
        }

        // POST: TripEquipments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TripEquipment tripEquipment = db.TripEquipments.Find(id);
            db.TripEquipments.Remove(tripEquipment);
            db.SaveChanges();
            return RedirectToAction("Index", "TripEquipments", new { id = tripEquipment.TripId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
