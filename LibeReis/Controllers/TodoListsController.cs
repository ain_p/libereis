﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibeReis.Models;

namespace LibeReis.Controllers
{
    [Authorize]
    public class TodoListsController : MyController
    {

        // GET: TodoLists
        public ActionResult Index(int id)
        {
            CheckPerson();
            //var todoLists = db.TodoLists.Include(t => t.Trip);
            var todoList = db.TodoLists
                .Where(t => t.TripId == id && t.Trip.People.Any(p => p.Id == CurrentPerson.Id))
                .ToList();
            ViewBag.CurrentPersonId = CurrentPerson.Id;
            ViewBag.OwnerId =  GetOwnerId(id);
            ViewBag.TripId = id;
            return View(todoList);
        }

        // GET: TodoLists/Details/5
        public ActionResult Details(int id)
        {
            CheckPerson();
            var ownerId = GetOwnerId(id);
            if (CurrentPerson.Id == ownerId)
            {
                ViewBag.TripId = id;
                return HttpNotFound();
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodoList todoList = db.TodoLists.Find(id);
            if (todoList == null)
            {
                return HttpNotFound();
            }
            return View(todoList);
        }

        // GET: TodoLists/Create
        public ActionResult Create(int id)
        {
            CheckPerson();
            var ownerId = GetOwnerId(id);
            if (CurrentPerson.Id == ownerId)
            {
                //ViewBag.TripId = new SelectList(db.Trips, "Id", "Name");
                ViewBag.TripId = id;
            }
            
            return View();
        }

        // POST: TodoLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,OwnerId,Status")] TodoList todoList)
        {
            CheckPerson();
            var ownerId = GetOwnerId(todoList.Id);
            todoList.TripId = todoList.Id;
            if (ModelState.IsValid && CurrentPerson.Id == ownerId)
            {
                var trip = db.Trips.Find(todoList.TripId);
                trip.TodoLists.Add(todoList);
                todoList.OwnerId = CurrentPerson.Id;
                db.SaveChanges();
                return RedirectToAction("Index", "ToDoLists", new { id = todoList.TripId });
            }
            ViewBag.TripId = todoList.TripId;
            //ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", todoList.TripId);
            return View(todoList);
        }

        // GET: TodoLists/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodoList todoList = db.TodoLists.Find(id);
            if (todoList == null)
            {
                return HttpNotFound();
            }
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", todoList.TripId);
            return View(todoList);
        }

        // POST: TodoLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TripId,Name,Description,OwnerId,Status")] TodoList todoList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(todoList).State = EntityState.Modified;
                ViewBag.TripId = todoList.TripId;
                db.SaveChanges();
                return RedirectToAction("Index", "TodoLists", new { id = ViewBag.TripId });
            }
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", todoList.TripId);
            return View("Index");
        }

        // GET: TodoLists/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodoList todoList = db.TodoLists.Find(id);
            if (todoList == null)
            {
                return HttpNotFound();
            }
            ViewBag.TripId = todoList.TripId;
            return View(todoList);
        }

        // POST: TodoLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TodoList todoList = db.TodoLists.Find(id);
            db.TodoLists.Remove(todoList);
            ViewBag.TripId = todoList.TripId;
            db.SaveChanges();
            return RedirectToAction("Index", new { id = ViewBag.TripId});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
