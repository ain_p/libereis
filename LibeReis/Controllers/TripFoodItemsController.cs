﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LibeReis.Models;

namespace LibeReis.Controllers
{
    public class TripFoodItemsController : MyController
    {
        // GET: TripFoodItems
        public ActionResult Index(int? tripId, int? tripDayId, int? foodType)
        {
            var tripFoodItems = db.TripFoodItems.Where(t => t.TripDayId == tripDayId && t.FoodType == foodType && t.TripId == tripId)
                .Include(t => t.FoodItem)
                .Include(t => t.Trip)
                .Include(t => t.TripDay);
            ViewBag.FoodTypes = TripFoodItem.FoodTypes;
            ViewBag.TripId = tripId;
            ViewBag.FoodType = foodType;
            ViewBag.TripDayId = tripDayId;
            return View(tripFoodItems.ToList());
        }

        // GET: TripFoodItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripFoodItem tripFoodItem = db.TripFoodItems.Find(id);
            if (tripFoodItem == null)
            {
                return HttpNotFound();
            }
            return View(tripFoodItem);
        }

        // GET: TripFoodItems/Create
        public ActionResult Create(int tripId, int tripDayId, int? foodType)
        {
            CheckPerson();
            var trip = db.Trips.Find(tripId);
            if (trip == null
                || trip.OwnerId != CurrentPerson.Id
                //|| !TripFoodItem.FoodTypes.ContainsKey(foodType)
                || !trip.TripDays.Any(t => t.Id == tripDayId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }



            TripFoodItem tripFoodItem = new TripFoodItem
            {
                TripId = tripId,
                TripDayId = tripId,
                //FoodType = foodType
            };
            ViewBag.FoodItemId = new SelectList(db.FoodItems, "Id", "Name");
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name");
            ViewBag.TripDayId = new SelectList(db.TripDays, "Id", "Name");
            ViewBag.FoodType = foodType;
            ViewBag.AmountUnit = new SelectList(TripFoodItem.AmountUnits, "Value", "Value");
            //ViewBag.FoodTypes = new SelectList(TripFoodItem.FoodTypes, "Key", "Value");
            return View(tripFoodItem);
        }

        // POST: TripFoodItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TripId,TripDayId,FoodType,FoodItemId,AmountUnit,Amount,Weight,Price,CarrierId")] TripFoodItem tripFoodItem)
        {
            if (ModelState.IsValid)
            {
                db.TripFoodItems.Add(tripFoodItem);
                db.SaveChanges();
                return RedirectToAction("Index", new { tripId = tripFoodItem.TripId, tripDayId = tripFoodItem.TripDayId, foodType = tripFoodItem.FoodType });
            }

            ViewBag.FoodItemId = new SelectList(db.FoodItems, "Id", "Name", tripFoodItem.FoodItemId);
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", tripFoodItem.TripId);
            ViewBag.TripDayId = new SelectList(db.TripDays, "Id", "Name", tripFoodItem.TripDayId);
            ViewBag.FoodType = tripFoodItem.FoodType;
            //ViewBag.FoodTypes = new SelectList(TripFoodItem.FoodTypes, "Key", "Value", tripFoodItem.FoodType);
            return View(tripFoodItem);
        }

        // GET: TripFoodItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripFoodItem tripFoodItem = db.TripFoodItems.Find(id);
            if (tripFoodItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.FoodItemId = new SelectList(db.FoodItems, "Id", "Name", tripFoodItem.FoodItemId);
            ViewBag.TripId = tripFoodItem.TripId; //new SelectList(db.Trips, "Id", "Name", tripFoodItem.TripId);
            //ViewBag.TripDayId = new SelectList(db.TripDays, "Id", "Name", tripFoodItem.TripDayId);
            ViewBag.TripDayId = tripFoodItem.TripDayId;
            return View(tripFoodItem);
        }

        // POST: TripFoodItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TripId,TripDayId,FoodType,FoodItemId,AmountUnit,Amount,Weight,Price,CarrierId")] TripFoodItem tripFoodItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tripFoodItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { tripId = tripFoodItem.TripId, tripDayId = tripFoodItem.TripDayId, foodType = tripFoodItem.FoodType });
            }
            ViewBag.FoodItemId = new SelectList(db.FoodItems, "Id", "Name", tripFoodItem.FoodItemId);
            ViewBag.TripId = new SelectList(db.Trips, "Id", "Name", tripFoodItem.TripId);
            ViewBag.TripDayId = new SelectList(db.TripDays, "Id", "Name", tripFoodItem.TripDayId);
            return View(tripFoodItem);
        }

        // GET: TripFoodItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TripFoodItem tripFoodItem = db.TripFoodItems.Find(id);
            if (tripFoodItem == null)
            {
                return HttpNotFound();
            }
            return View(tripFoodItem);
        }

        // POST: TripFoodItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TripFoodItem tripFoodItem = db.TripFoodItems.Find(id);
            db.TripFoodItems.Remove(tripFoodItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
