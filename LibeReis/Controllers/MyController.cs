﻿using LibeReis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;

namespace LibeReis.Controllers
{
    public class MyController : Controller
    {
        protected ReisEntities db = new ReisEntities();
        public Person CurrentPerson = null; // siis on hea kohe see PersonObject omale taskusse panna


        // võtsin selle rea ära, et saaks erinevates andmebaasides kolada
        //[OutputCache(Duration =3600, VaryByParam = "id", Location = System.Web.UI.OutputCacheLocation.Any)]
        public ActionResult Content(int? id)
        {

            DataFile df = db.DataFiles.Find(id);
            if (df == null) return HttpNotFound();
            return File(df.Content, df.ContentType);
        }

        //protected Person CurrentPerson = null; // siis on hea kohe see PersonObject omale taskusse panna
        //protected void CheckPerson()
        //{

        //    if (Request.IsAuthenticated)        // kontrollida vaja vaid siis
        //        if ((CurrentPerson?.EMail ?? "") != User.Identity.Name)    // kui juba ei ole meeles või on vale
        //        {
        //            CurrentPerson = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();
        //            if (CurrentPerson == null)
        //            {
        //                using (ApplicationDbContext dba = new ApplicationDbContext())
        //                {
        //                    ApplicationUser u = dba.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
        //                    db.People.Add(CurrentPerson = new Person
        //                    {
        //                        EMail = User.Identity.Name,
        //                        FirstName = u.FirstName,
        //                        LastName = u.LastName,
        //                        BirthDate = u.BirthDate
        //                    });
        //                    db.SaveChanges();
        //                }
        //            }
        //        }
        //        else CurrentPerson = null;
        //}

        // file - kontrollerisse post-requestiga üles laetud fail
        // oldId - vana faili number, mis baasist kustutada (null - mitte kustutada
        // change - tegevus, mis lisamise ja kustutamise vahel vaja teha
        //  x => {person.PictureId = x; db.SaveChanges();}
        protected void ChangeDataFile(HttpPostedFileBase file, Action<int> change, int? oldId)
        {
            if (file != null && file.ContentLength > 0)
                using (BinaryReader br = new BinaryReader(file.InputStream))
                {
                    DataFile df = new DataFile
                    {
                        Content = br.ReadBytes(file.ContentLength),
                        FileName = file.FileName.Split('\\').Last().Split('/').Last(),
                        ContentType = file.ContentType,
                        Created = DateTime.Now
                    };
                    db.DataFiles.Add(df);
                    db.SaveChanges();
                    change(df.Id);
                    if (oldId.HasValue)
                    {
                        db.DataFiles.Remove(db.DataFiles.Find(oldId.Value));
                        db.SaveChanges();
                    }

                }
        }
        public void CheckPerson()
        {
            if (Request.IsAuthenticated)        // kontrollida vaja vaid siis
                if ((CurrentPerson?.Email ?? "") != User.Identity.Name)    // kui juba ei ole meeles või on vale
                    using (ReisEntities dbr = new ReisEntities())
                    {
                        CurrentPerson = dbr.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                        if (CurrentPerson == null)
                        {
                            using (ApplicationDbContext dbu = new ApplicationDbContext())
                            {
                                ApplicationUser u = dbu.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                                dbr.People.Add(CurrentPerson = new Person
                                {
                                    Email = User.Identity.Name,
                                    FirstName = u.FirstName,
                                    LastName = u.LastName,
                                    PhoneNumber = u.PhoneNumber ?? ""
                                }); ;
                                dbr.SaveChanges();
                            }
                        }
                    }
                else CurrentPerson = null;
        }

        protected int? GetOwnerId(int? tripId)
        {
            return db.Trips.Find(tripId)?.OwnerId??null;
        }
    }
}