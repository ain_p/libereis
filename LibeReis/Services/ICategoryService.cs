﻿using LibeReis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibeReis.Services
{
    public interface ICategoryService
    {
        IEnumerable<TripCategory> GetCategories();
        IEnumerable<TripSubcategory> GetSubCategories(int categoryId);
    }
}
