﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibeReis.Models;

namespace LibeReis.Services
{
    public class CategoryService : ICategoryService
    {
        private ReisEntities db = new ReisEntities();
        public IEnumerable<TripCategory> GetCategories()
        {
            return db.TripCategories;
        }

        public IEnumerable<TripSubcategory> GetSubCategories(int categoryId)
        {
            return db.TripSubcategories.Where(x => x.CategoryId == categoryId).ToList();
        }
    }
}