﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace LibeReis.Models
{
    [MetadataType(typeof(DataFileAnnotations))]
    public partial class DataFile
    {
    }

    [MetadataType(typeof(EquipmentTypeAnnotations))]
    public partial class EquipmentType
    {
    }

    [MetadataType(typeof(FoodItemAnnotations))]
    public partial class FoodItem
    {
    }

    [MetadataType(typeof(PersonAnnotations))]
    public partial class Person
    {
        public string FullName => $"{FirstName} {LastName}";
    }

    [MetadataType(typeof(SpendingAnnotations))]
    public partial class Spending
    {
    }

    [MetadataType(typeof(TodoListAnnotations))]
    public partial class TodoList
    {
    }

    [MetadataType(typeof(TripAnnotations))]
    public partial class Trip
    {
    }

    [MetadataType(typeof(TripCategoryAnnotations))]
    public partial class TripCategory
    {
        public static List<TripCategory> FetchTripSubcategory(string categoryName)
        {
            return new List<TripCategory> { };
        }
    }

    [MetadataType(typeof(TripDayAnnotations))]
    public partial class TripDay
    {
    }

    [MetadataType(typeof(TripEquipmentAnnotations))]
    public partial class TripEquipment
    {
    }

    [MetadataType(typeof(TripFoodItemAnnotations))]
    public partial class TripFoodItem
    {
        public static Dictionary<int, string> FoodTypes = new Dictionary<int, string>
        {
            {0, "Hommikusöök" },
            {1, "Lõunasöök" },
            {2, "Õhtusöök" },
        };
        public static Dictionary<int, string> AmountUnits = new Dictionary<int, string>
        {
            {0, "g" },
            {1, "tk" },
        };
    }

    [MetadataType(typeof(TripSubcategoryAnnotations))]
    public partial class TripSubcategory
    {
    }

    public partial class ReisEntities
    {
        //public override int SaveChanges()
        //{
        //    try
        //    {
        //        return base.SaveChanges();
        //    }
        //    catch (DbEntityValidationException e)
        //    {
        //        var errorMessages = e.EntityValidationErrors
        //                .SelectMany(x => x.ValidationErrors)
        //                .Select(x => x.ErrorMessage);

        //        var fullErrorMessage = string.Join(" " , errorMessages);
        //        var exceptionMessage = string.Concat(e.Message, "Validation errors: ", fullErrorMessage);
        //        throw new DbEntityValidationException(exceptionMessage, e.EntityValidationErrors);
        //    }
        //}

    }
}