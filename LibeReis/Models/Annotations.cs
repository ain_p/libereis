﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibeReis.Models
{
    public class DataFileAnnotations
    {

    }

    public class EquipmentTypeAnnotations
    {
        [Required]
        [MaxLength(128)]
        public string Name;

        [MaxLength(128)]
        [Display(Name = "Local name")]
        public string LocalName;

        [MaxLength(1024)]
        public string Description;
    }

    public class FoodItemAnnotations
    {
        [Required]
        [MaxLength(128)]
        public string Name;

        [MaxLength(128)]
        [Display(Name = "Local name")]
        public string LocalName;

        [MaxLength(1024)]
        public string Description;
    }

    public class SpendingAnnotations
    {
        [Required]
        [MaxLength(128)]
        public string Name;
    }

    public class TodoListAnnotations
    {
        [Required]
        [MaxLength(128)]
        public string Name;

        [MaxLength(2048)]
        public string Description;
    }

    public class TripAnnotations
    {
        [Required]
        [MaxLength(128)]
        [Display(Name = "Trip name")]
        public string Name;

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? StartDate;

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? EndDate;
    }

    public class TripCategoryAnnotations
    {
        [Required]
        [MaxLength(128)]
        [Display(Name = "Category")]
        public string Name;

        [MaxLength(128)]
        [Display(Name = "Local name")]
        public string LocalName;
    }

    public class TripDayAnnotations
    {
        [Required]
        [MaxLength(128)]
        public string Name;

        [MaxLength(65536)]
        [DataType(DataType.MultilineText)]
        public string Description;

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? TripDay1;
    }

    public class PersonAnnotations
    {
        [Display(Name = "Given name")]
        public string FirstName;
        [Display(Name = "Family name")]
        public string LastName;
        [Display(Name = "Phone number")]
        public string PhoneNumber;
        [Display(Name = "Name")]
        public string FullName;
    }

    public class TripEquipmentAnnotations
    {

    }

    public class TripFoodItemAnnotations
    {

    }

    public class TripMemberAnnotations
    {

    }

    public class TripSubcategoryAnnotations
    {
        [Required]
        [MaxLength(128)]
        [Display(Name = "Subcategory")]
        public string Name;

        [MaxLength(128)]
        [Display(Name = "Local name")]
        public string LocalName;

    }
}