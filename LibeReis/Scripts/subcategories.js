﻿function changeCategory(category) {
    var subCat = $("#TripSubcategoryId");
    subCat.empty();
    subCat.append("<option value=''>-- vali alamliik --</option>");
    $.ajax({
        url: "/Trips/Subcategories?catId=" + category.value,
        success: function (data) {
            jsonData = JSON.parse(data);
            for (i in jsonData) {
                subCat.append(`<option value="${jsonData[i].Id}">${jsonData[i].Name}</option>`);
            }
        }
    }
    );
};

